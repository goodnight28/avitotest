import Foundation

struct Structure: Codable {
    let id: Int!
    var title: String?
    var value: ValueUnion?
    var values: [Structure]?
    var params: [Structure]?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case value = "value"
        case values = "values"
        case params = "params"
    }
}

enum ValueUnion: Codable{
    case integer(Int)
    case string(String)
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(ValueUnion.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for ValueUnion"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
    
    func getValue() -> Any{
        switch self {
        case .integer(let x):
            return x
        case .string(let x):
            return x
        }
    }
}

