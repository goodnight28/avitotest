import Foundation

struct Error: Codable {
    let error = ["message": "Входные файлы некорректны"]
    
    enum CodingKeys: String, CodingKey {
        case error = "error"
    }
}
