import Foundation

enum NameFile: String {
    case structure = "Structure"
    case draft_values = "Draft_values"
    case structure_with_values = "Structure_with_values"
    case error = "error"
}
