import Foundation

class Find {
    
    public func findInStructure(structure: inout Structure, draftValues: Structure) {
        guard let values_draftValues = draftValues.values else { return }
        
        for counter in 0..<values_draftValues.count {
            guard let id_draftValues = values_draftValues[counter].id,
                  let value_draftValues = values_draftValues[counter].value else { continue }
            
            findInArray(structure: &structure, id: id_draftValues, value: value_draftValues)
        }
    }
    
    //MARK: Данная функция проверяет массивы на существования
    private func findInArray (structure: inout Structure, id: Int, value: ValueUnion)  {
        if structure.params != nil {
            array(structureArray: &structure.params!, id: id, value: value)
        } else if structure.values != nil {
            array(structureArray: &structure.values!, id: id, value: value)
        }
    }
    
    /* MARK: Функция ищет id в массиве и если его не находит, то идет на следующий уровень
     * Если id был найден, то определяет тип переменной value
     * Если value имеет тип String, то записываем его
     * Если value имеет тип Int, то ищем кому принадлежит value
     */
    private func array(structureArray: inout [Structure], id: Int, value: ValueUnion) {
        if let indexId = structureArray.firstIndex(where: {$0.id == id}) {
            if type(of: value.getValue()) == String.self {
                structureArray[indexId].value = value
            } else {
                if structureArray[indexId].values != nil {
                    if let nextStructure = structureArray[indexId].values!.first(where: {$0.id == value.getValue() as? Int}) {
                        structureArray[indexId].value = ValueUnion.string(nextStructure.title ?? "")
                    }
                } else if structureArray[indexId].params != nil {
                    if let nextStructure = structureArray[indexId].params!.first(where: {$0.id == value.getValue() as? Int}) {
                        structureArray[indexId].value = ValueUnion.string(nextStructure.title ?? "")
                    }
                }
            }
        } else {
            for counter in 0..<structureArray.count {
                findInArray(structure: &structureArray[counter], id: id, value: value)
            }
        }
    }
}
