import UIKit
import Foundation

public class Main{
    
    private let json = Json()
    private let find = Find ()
    
    private var structure: Structure!
    private var draftValues: Structure!
    
    public init(){
        structure = json.readFromJsonFile(NameFile.structure.rawValue)
        draftValues = json.readFromJsonFile(NameFile.draft_values.rawValue)
    }
    
    public func start(){
        find.findInStructure(structure: &structure, draftValues: draftValues)
        json.saveToJsonFile(NameFile.structure_with_values.rawValue, structure: structure)
    }
}
