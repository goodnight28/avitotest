import UIKit

//MARK: Класс по работе с JSON файлами
class Json {
    
    public func saveToJsonFile<T: Codable>(_ fileName: String, structure: T) {
        //MARK: Получает url .json файла в каталоге документов
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("\(fileName).json")
        
        guard let stream = OutputStream(toFileAtPath: fileUrl.path, append: false) else { return }
        stream.open()
        defer {
            stream.close()
        }
        
        do {
            let jsonData = try JSONEncoder().encode(structure)
            let json = try JSONSerialization.jsonObject(with: jsonData, options: [])
            
            var error: NSError?
            JSONSerialization.writeJSONObject(json, to: stream, options:[], error: &error)
            
            print("Путь к файлу -> \(fileUrl)")
            
            if let error = error {
                print(error)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    public func readFromJsonFile(_ fileName: String) -> Structure? {
        //MARK: Получает url .json файла в каталоге документов
        let fileUrl = Bundle.main.url(forResource:"\(fileName)", withExtension: "json")
        
        //MARK: Читает данные из файла и преобразовывает их в Structure
        do {
            let data = try Data(contentsOf: fileUrl!)
            let structure = try JSONDecoder.init().decode(Structure.self, from: data)
            
            return structure
        } catch {
            saveToJsonFile(NameFile.error.rawValue, structure: Error())
            print("\nError -> \(error)")
            return nil
        }
    }
}
